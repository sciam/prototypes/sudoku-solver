# Sudoku Solver

Sudoku solver using prolog.

## Running this project.


Command for executing the solver :

```
?- statistics(walltime, [TimeSinceStart | [TimeSinceLastCall]]),
   problem(1,X),
   problem(1,L),
   sudoku(L),
   statistics(walltime, [NewTimeSinceStart | [ExecutionTime]]),
   write('Execution took '), write(ExecutionTime), write(' ms.'), nl.
```

## References

Prolog documentation: https://github.com/SWI-Prolog/swipl-devel/blob/03fa166e4f15d466904e7a0440f5b56a26d59d21/library/clp/clpfd.pl#L501

Research Paper: https://hal-lirmm.ccsd.cnrs.fr/lirmm-01897933/document

