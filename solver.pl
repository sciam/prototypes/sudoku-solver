:- use_rendering(sudoku).
:- use_module(library(clpfd)).

sudoku(L) :- 
    length(L, 9),
    append(L, Vs), Vs ins 1..9,
    solve(L).
    
solve([ [A0, A1, A2, A3, A4, A5, A6, A7, A8],
        [B0, B1, B2, B3, B4, B5, B6, B7, B8],
        [C0, C1, C2, C3, C4, C5, C6, C7, C8],
        [D0, D1, D2, D3, D4, D5, D6, D7, D8],
        [E0, E1, E2, E3, E4, E5, E6, E7, E8],
        [F0, F1, F2, F3, F4, F5, F6, F7, F8],
        [G0, G1, G2, G3, G4, G5, G6, G7, G8],
        [H0, H1, H2, H3, H4, H5, H6, H7, H8],
        [I0, I1, I2, I3, I4, I5, I6, I7, I8]]):-
    
    % verify lines
    all_distinct([A0, A1, A2, A3, A4, A5, A6, A7, A8]),
    all_distinct([B0, B1, B2, B3, B4, B5, B6, B7, B8]),
    all_distinct([C0, C1, C2, C3, C4, C5, C6, C7, C8]),
    all_distinct([D0, D1, D2, D3, D4, D5, D6, D7, D8]),
    all_distinct([E0, E1, E2, E3, E4, E5, E6, E7, E8]),
    all_distinct([F0, F1, F2, F3, F4, F5, F6, F7, F8]),
    all_distinct([G0, G1, G2, G3, G4, G5, G6, G7, G8]),
    all_distinct([H0, H1, H2, H3, H4, H5, H6, H7, H8]),
    all_distinct([I0, I1, I2, I3, I4, I5, I6, I7, I8]),
    
    % verify columns
    all_distinct([A0, B0, C0, D0, E0, F0, G0, H0, I0]),
    all_distinct([A1, B1, C1, D1, E1, F1, G1, H1, I1]),
    all_distinct([A2, B2, C2, D2, E2, F2, G2, H2, I2]),
    all_distinct([A3, B3, C3, D3, E3, F3, G3, H3, I3]),
    all_distinct([A4, B4, C4, D4, E4, F4, G4, H4, I4]),
    all_distinct([A5, B5, C5, D5, E5, F5, G5, H5, I5]),
    all_distinct([A6, B6, C6, D6, E6, F6, G6, H6, I6]),
    all_distinct([A7, B7, C7, D7, E7, F7, G7, H7, I7]),
    all_distinct([A8, B8, C8, D8, E8, F8, G8, H8, I8]),
    
    % verify squares
    all_distinct([A0, A1, A2, B0, B1, B2, C0, C1, C2]),
    all_distinct([A3, A4, A5, B3, B4, B5, C3, C4, C5]),
    all_distinct([A6, A7, A8, B6, B7, B8, C6, C7, C8]),
    all_distinct([D0, D1, D2, E0, E1, E2, F0, F1, F2]),
    all_distinct([D3, D4, D5, E3, E4, E5, F3, F4, F5]),
    all_distinct([D6, D7, D8, E6, E7, E8, F6, F7, F8]),
    all_distinct([G0, G1, G2, H0, H1, H2, I0, I1, I2]),
    all_distinct([G3, G4, G5, H3, H4, H5, I3, I4, I5]),
    all_distinct([G6, G7, G8, H6, H7, H8, I6, I7, I8]).

problem(1,[[7,6,_,_,_,9,_,1,_],
           [8,_,_,_,_,3,7,2,4],
           [_,_,_,7,1,4,_,_,9],
           [4,_,6,_,_,_,3,_,2],
           [_,7,_,4,5,6,_,_,_],
           [_,5,1,_,_,_,_,7,6],
           [1,_,_,2,9,_,_,4,_],
           [2,_,7,6,3,_,1,_,_],
           [_,9,8,_,_,5,2,_,7]]).


